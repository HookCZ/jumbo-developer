#!/usr/bin/env node
"use strict";

/************* Requires ***************/
const readline = require("readline");
const commands = require("./commands");


/************* Definitions ***************/
// Console interface
const CLI = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: true,
	prompt: "\x1b[1mJumbo Developer>\x1b[0m ",
	completer: function(line) {
		if (line.trim().length === 0) return [null, line];
		let hits = commands.availableCommands.filter((c) => { return c.indexOf(line) === 0 });
		return [hits, line];
	}
});


/************* Program ***************/
console.log("\n---------------------------------------");
console.log("Jumbo Developer CLI");
console.log("Type 'help' to list available commands");
console.log("----------------------------------------\n");
commands.initDeveloper(CLI);

// Set event
CLI.on("line", function(inputText) {
	if (inputText) {
		let cmds = [];

		// Get parts of command
		inputText.replace(/(?:([^"]+?)|(".+?"))(?:\s|$)/g, function(_, idLike, strLike) {
			if (idLike) {
				cmds.push(idLike);
			} else if (strLike) {
				cmds.push(strLike.slice(1, strLike.length - 2));
			}
		});

		let cmd = cmds[0];
		let args = cmds.slice(1);

		// switch by main command
		switch (cmd) {
			case "help": commands.cmdHelp(); return;
			case "v":
			case "version": commands.cmdVersion(); return;
			case "exit": commands.cmdExit(); return;
			case "c":
			case "create": commands.cmdCreate(args); return;
			// case "role-manager": commands.cmdRoleManager(); return;
			default:
				console.log("You've entered unrecognized command '" + cmds[0] + "'");
				break;
		}
	}

	CLI.prompt();

	// Fix prompt disappearing
	readline.moveCursor(CLI, 0, 0);
});

// Init console reading
CLI.prompt();
