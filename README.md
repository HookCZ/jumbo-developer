# Jumbo-developer #

Developer utility for [JumboJS](https://www.npmjs.com/package/jumbo-core) (jumbo-core package). This utility helps you create controllers, actions, services and more. Include CLI for base actions and GUI for user role management.

In progress.

## Installation
```npm install jumbo-developer -g```

## Start from cmd
```
jumbo-developer
```
