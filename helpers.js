"use strict";

//region Imports

const $fs = require("fs");
const $path = require("path");
const readline = require("readline");
const Colors = require("./colors");

//endregion

exports.projectExists = false;

/**
 * Log colored message
 * @param {Colors | string} color
 * @param args
 */
function logColor(color, ...args) {
	args.unshift(color);
	args.push(Colors.Reset);
	console.log.apply(null, args);
}
exports.logColor = logColor;

/**
 * New prompt
 */
function prompt() {
	jdev.CLI.prompt();

	// Fix prompt disappearing
	readline.moveCursor(jdev.CLI, 0, 0);
}
exports.prompt = prompt;

/**
 * Check if important project folders exist; It'll log error if not and return false;
 * @returns {boolean} True if exists
 */
function checkProjectExists() {
	if (!jdev.CLI) return false;
	if (exports.projectExists) return true; // Jumbo-developer already check that and found project

	exports.projectExists = $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app"))
		&& $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app", "controllers"))
		&& $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app", "services"))
		&& $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app", "models"))
		&& $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app", "templates"))
		&& $fs.existsSync($path.resolve(jdev.PROJ_DIR, "app", "sub-apps"));

	if (!exports.projectExists) {
		console.log("Project not found in defined directory. You should create project with 'create project' before you start developing.");
		return false;
	}

	return true;
}
exports.checkProjectExists = checkProjectExists;

/**
 * Copy whole directory
 * @param src
 * @param dest
 * @returns {*}
 */
function copyDir(src, dest) {
	try {
		try {
			$fs.mkdirSync(dest, 0o755);
		} catch (ex) {
			if (ex.code !== "EEXIST") {
				// noinspection ExceptionCaughtLocallyJS
				throw ex;
			}
		}

		let files = $fs.readdirSync(src);
		let errors = 0;

		for (let i = 0; i < files.length; i++) {
			if (files[i] === "." || files[i] === "..") continue;

			let current = $fs.lstatSync($path.join(src, files[i]));

			if (current.isDirectory()) {
				errors += copyDir($path.join(src, files[i]), $path.join(dest, files[i])) ? 0 : 1;
			} else if (current.isSymbolicLink()) {
				let symlink = $fs.readlinkSync($path.join(src, files[i]));
				$fs.symlinkSync(symlink, $path.join(dest, files[i]));
			} else {
				$fs.createReadStream($path.join(src, files[i])).pipe($fs.createWriteStream($path.join(dest, files[i])));
			}
		}

		return errors;
	} catch (ex) {
		console.log("Some error occurs while copying ", $path.relative("./project-base", src), ex.message);
		return false;
	}
}
exports.copyDir = copyDir;

/**
 * Create nested directories
 * @param dir
 */
function createDir(dir) {
	try {
		$fs.mkdirSync(dir, 0o777);
	} catch (e) {
		if (e.code === "ENOENT") {
			createDir($path.dirname(dir));
			createDir(dir);
		} else {
			console.error(e.message);
		}
	}
}
exports.createDir = createDir;


const CTOR_PARAMS_MATCH_REGEX = /constructor\s*\((?:[^), ]+(?:\s*,\s*)?)*\)/;
const CTOR_BODY_MATCH_REGEX = /constructor\s*\((?:[^), ]+(?:\s*,\s*)?)*\)\s*{(\s*)(super\(.*?\);)?/;

/**
 * Ask for injectation
 * @param templateContent
 * @param callback
 */
function injectServices(templateContent, callback) {
	jdev.CLI.question("\nInject services (separated by ','): ", function (services) {
		let serviceNames = (services || "").trim().split(/,\s?/).filter(s => s.length > 0);
		let ctorMatch = templateContent.match(CTOR_PARAMS_MATCH_REGEX);

		if (serviceNames.length) {
			if (ctorMatch) {
				// Inject services into ctor
				let beforeEndBracketPos = ctorMatch.index + ctorMatch[0].length - 1;
				templateContent = templateContent.slice(0, beforeEndBracketPos)
					+ serviceNames.join(", ")
					+ templateContent.slice(beforeEndBracketPos);

				// Store it in fields
				// match ctor body
				let ctorBodyMatch = templateContent.match(CTOR_BODY_MATCH_REGEX);

				if (ctorBodyMatch) {
					let bodyStartIndex = ctorBodyMatch.index + ctorBodyMatch[0].length;
					let space = ((ctorBodyMatch[1] || "\t").match(/^[ \t]+/m) || ["\t"])[0];
					let extraIndent = templateContent.charAt(bodyStartIndex) === "}" ? "\t" : "";

					let fieldsAssignment = "", service;
					for (let i = 0; i < serviceNames.length; i++) {
						service = serviceNames[i];
						fieldsAssignment += extraIndent
							+ "this." + service.charAt(0).toLowerCase() + service.slice(1)
							+ " = " + service + ";\n" + space;
					}

					templateContent = templateContent.slice(0, bodyStartIndex)
						+ fieldsAssignment
						+ templateContent.slice(bodyStartIndex);
				} else {
					console.warn("Unable to detect ctor body to assign services to fields.");
				}
			} else {
				console.warn("Unable to inject services.");
			}
		}

		callback(templateContent);
	});
}
exports.injectServices = injectServices;
