"use strict";

const $path = require("path");
const {prompt} =  require("../helpers");

module.exports = function cmdVersion(jumboVer, developerVer) {
	if (!jdev.CLI) return;

	if (!jumboVer) {
		try {
			jumboVer = require($path.join(jdev.PROJ_DIR, "node_modules", "jumbo-core", "package.json")).version;
		} catch (e) {
			jumboVer = " Unknown";
		}
	}

	if (!developerVer) {
		try {
			developerVer = require($path.join(jdev.DEV_DIR, "package.json")).version;
		} catch (e) {
			developerVer = " Unknown";
		}
	}

	console.log("\nJumbo v" + jumboVer);
	console.log("Jumbo-developer v" + developerVer, "\n");

	prompt();
};