"use strict";

const $opn = require("opn");
const $path = require("path");
const {prompt} =  require("../helpers");

module.exports = function cmdRoleManager() {
	if (!jdev.CLI) return;
	$opn($path.join(__dirname, "role-manager", "role-manager.html"));
	prompt();
};