"use strict";

const $fs = require("fs");
const $path = require("path");

const Colors = require("../../colors");
const {injectServices, prompt, checkProjectExists, logColor} = require("../../helpers");

/**
 * Create controller in application and optionally in sub-app
 * @param args
 */
module.exports = function createController(args) {
	if (!checkProjectExists()) return;

	let controllerName = args[0];
	controllerName = controllerName[0].toUpperCase() + controllerName.slice(1);
	let controller = controllerName + "Controller";
	let subapp = args[1] > 0 ? $path.join("sub-apps", args[1]) : "";
	let path = $path.resolve(jdev.PROJ_DIR, "app", subapp, "controllers", controller + jdev.CLASS_EXTENSION);

	if ($fs.existsSync(path)) {
		console.log("Controller already exists.");
		prompt();
		return;
	}

	jdev.CLI.question("\nDo you really want to create " + path + "? (y/n): ", function (confirm) {
		if (confirm !== "y") {
			prompt();
			return;
		}

		try {
			// read controller template
			let controllerTemplateContent = $fs.readFileSync($path.resolve(jdev.DEV_DIR, "templates", "controller"
				+ jdev.CODE_TEMPLATE_EXTENSION));

			// replace controller name
			controllerTemplateContent = controllerTemplateContent.toString().replace(/__ControllerName__/g, controller);

			// Construct namespace
			let namespace = "App.";
			if (subapp.length > 0) {
				namespace += "SubApps." + subapp + ".";
			}
			namespace += "Controllers";

			// replace namespace
			controllerTemplateContent = controllerTemplateContent.toString().replace(/__Namespace__/g, namespace);

			// Proc service injectation
			injectServices(controllerTemplateContent, function(controllerTemplateContent) {
				// write template content to created file
				$fs.writeFileSync(path, controllerTemplateContent);

				let tpl = $path.join(jdev.PROJ_DIR, "app", subapp, "templates", controllerName);

				// Create folder in templates
				$fs.mkdirSync(tpl, 0o755);

				// Create template for default index action
				$fs.closeSync($fs.openSync($path.join(tpl, "index" + jdev.TEMPLATE_EXTENSION), "w"));

				logColor(Colors.FgGreen, "\nController was successfully created.\n");

				prompt();
			});
		} catch (ex) {
			console.error("Error occurs while creating file.", ex);
			prompt();
		}

	});
};