"use strict";

const $path = require("path");
const $exec = require("child_process").exec;

const {prompt, checkProjectExists, copyDir} = require("../../helpers");
const projectExists = require("../../helpers").projectExists;

/**
 * Create project - copy structure
 * @param args
 */
module.exports = function createProject(args) {
	if (projectExists) {
		console.log("Project already exists.");
		prompt();
		return;
	}

	let noErrors = copyDir($path.resolve(jdev.DEV_DIR, "project-base"), jdev.PROJ_DIR);

	if (noErrors) {
		console.log("Project base successfully created. Running npm install...");

		if (checkProjectExists()) {
			// $npm.commands.install();
			$exec(`cd ${jdev.PROJ_DIR} && npm install`, (error, stdout) => {
				if (error) {
					console.error("npm install error", error.message);
					prompt();
					return;
				}

				// Show npm install output
				console.log(stdout);

				prompt();
			});
		}
	} else {
		console.log("Some error(s) occurs while creating project. Try resolve it manually.");
		prompt();
	}
};