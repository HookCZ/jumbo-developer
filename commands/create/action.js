"use strict";

const $fs = require("fs");
const $path = require("path");

const Colors = require("../../colors");
const {prompt, checkProjectExists, logColor } =  require("../../helpers");

/**
 * Create action in controller in application and optionally in sub-app
 * @param args
 */
module.exports = function createAction(args) {
	if (!checkProjectExists()) return;

	if (args.length < 2) {
		console.log("You must define action name and Controller name in which you want create that action.");
		console.log("Eg. 'create action registration UserAccount [sub-app name here optionally]'");
		prompt();
		return;
	}

	let actionId = args[0];
	let actionName = "action" + args[0][0].toUpperCase() + args[0].slice(1);
	let controller = args[1] + "Controller";
	let subapp = args[2] > 0 ? $path.join("sub-apps", args[2]) : "";

	let controllerPath = $path.resolve(jdev.PROJ_DIR, "app", subapp, "controllers", controller + jdev.CLASS_EXTENSION);
	let templatePath = $path.resolve(jdev.PROJ_DIR, "app", subapp, "templates", args[1]);

	try {
		let file = $fs.readFileSync(controllerPath, "utf-8");

		// Check class existance
		if (!file.match(/class\s+[\w\W]+\s*\{/)) {
			console.error("No class found in file.");
			return;
		}

		// Check action existance
		if (file.match(new RegExp("\\s" + actionName + "\\s*\(.*?\)\\s*\{"))) {
			console.error(`Action ${actionName} already exists.`);
			return;
		}

		jdev.CLI.question("\nDo you really want to create action " + actionName + " in " + controllerPath
			+ "? (y/n): ", function (confirm)
		{
			if (confirm !== "y") {
				prompt();
				return;
			}

			// Find line before last } bracket
			let lastBracketPos = null;
			file.replace(/\n\s*\}/g, function (_, index) {
				lastBracketPos = index;
			});

			if (!lastBracketPos) {
				console.error("No class ending bracket found.");
				prompt();
				return;
			}
			jdev.CLI.question("\nDo you want to create template for your new action? (y/n): ", function (confirm) {
				let actionTemplate;

				if (confirm !== "y") {

					// Read action template
					actionTemplate = $fs.readFileSync(
						$path.join(jdev.DEV_DIR, "templates", "action" + jdev.CODE_TEMPLATE_EXTENSION), "utf-8");
				} else {

					// Read action template returning view
					actionTemplate = $fs.readFileSync(
						$path.join(jdev.DEV_DIR, "templates", "action-view" + jdev.CODE_TEMPLATE_EXTENSION), "utf-8");

					// Add template
					$fs.createReadStream($path.join(jdev.DEV_DIR, "templates", "action" + jdev.TEMPLATE_TEMPLATE_EXTENSION))
						.pipe($fs.createWriteStream($path.join(templatePath, actionId + jdev.TEMPLATE_EXTENSION)));
				}

				// Replace __ActionName__
				actionTemplate = actionTemplate.replace("__ActionName__", actionName);

				// Add action to controller
				file = file.slice(0, lastBracketPos) + "\n\n" + actionTemplate + file.slice(lastBracketPos);

				// Save controller
				$fs.writeFileSync(controllerPath, file, "utf-8");

				logColor(Colors.FgGreen, "\nAction successfully added.\n");
				prompt();
			});
		});
	} catch (e) {
		logColor(Colors.FgRed,
			"\nController", controller, "(", controllerPath, ") does not exists or you cannot access some path.\n"
			+ e.message + "\n"
		);
		prompt();
	}
};