"use strict";

const $fs = require("fs");
const $path = require("path");

const Colors = require("../../colors");
const {prompt, checkProjectExists, logColor, copyDir } =  require("../../helpers");

/**
 * Create subapp
 * @param args
 */
module.exports = function createSubapp(args) {
	if (!checkProjectExists()) return;

	let subapp = args[0];
	let path = $path.resolve(jdev.PROJ_DIR, "app", "sub-apps", subapp);

	if ($fs.existsSync(path)) {
		console.log("Subapp already exists.");
		prompt();
		return;
	}

	jdev.CLI.question(`\nDo you really want to create subapp '${subapp}'? (y/n): `, function (confirm) {
		if (confirm !== "y") {
			prompt();
			return;
		}

		$fs.mkdirSync(path, 0o755);

		let noErrors = copyDir($path.resolve(jdev.DEV_DIR, "subapp-base"), path);

		if (noErrors) {
			logColor(Colors.FgGreen, "\nSubapp successfully created.\n");
		} else {
			logColor(Colors.FgRed, "\nSome error(s) occurs while creating subapp. Try resolve it manually.\n");
		}

		prompt();
	});

	prompt();
};