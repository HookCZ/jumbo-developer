"use strict";

const $fs = require("fs");
const $path = require("path");

const Colors = require("../../colors");
const {injectServices, prompt, checkProjectExists, logColor } =  require("../../helpers");

/**
 * Create facade in application and optionally in sub-app
 * @param args
 */
module.exports = function createFacade(args) {
	if (!checkProjectExists()) return;

	let name = args[0] || "";
	name = name[0].toUpperCase() + name.slice(1);
	let fullName = name + "Facade";
	let subapp = args[1] && args[1].length > 0 ? $path.join("sub-apps", args[1]) : "";
	let path = $path.resolve(jdev.PROJ_DIR, "app", subapp, "facades", fullName + jdev.CLASS_EXTENSION);

	if ($fs.existsSync(path)) {
		console.log("Facade already exists.");
		prompt();
		return;
	}

	jdev.CLI.question("\nDo you really want to create " + path + "? (y/n): ", function (confirm) {
		if (confirm !== "y") {
			prompt();
			return;
		}

		try {
			// read facad template
			let templateContent = $fs.readFileSync($path.resolve(jdev.DEV_DIR, "templates", "facade"
				+ jdev.CODE_TEMPLATE_EXTENSION));

			// replace controller name
			templateContent = templateContent.toString().replace(/__FacadeName__/g, fullName);

			// Construct namespace
			let namespace = "App.";
			if (subapp.length > 0) {
				namespace += "SubApps." + subapp + ".";
			}
			namespace += "Facades";

			// replace namespace
			templateContent = templateContent.toString().replace(/__Namespace__/g, namespace);

			// Proc service injectation
			injectServices(templateContent, function(templateContent) {
				// write template content to created file
				$fs.writeFileSync(path, templateContent);

				logColor(Colors.FgGreen, "\nFacade was successfully created.\n");

				prompt();
			});
		} catch (ex) {
			console.error("Error occurs while creating file.", ex);
			prompt();
		}
	});
};