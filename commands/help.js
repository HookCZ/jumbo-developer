"use strict";

const {prompt} =  require("../helpers");

module.exports = function cmdHelp() {
	if (!jdev.CLI) return;
	console.log("\nAvailable commands:\n\t", this.availableCommands.join(",\n\t"), "\n");
	prompt();
};