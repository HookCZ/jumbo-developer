"use strict";

const {prompt} =  require("../helpers");

module.exports = function cmdCreate(args) {
	if (!jdev.CLI) return;

	let what = args[0];
	args = args.slice(1);

	switch (what) {
		case "p":
		case "project":
			require("./create/project")(args);
			break;
		case "sub":
		case "subapp":
			require("./create/subapp")(args);
			break;
		case "c":
		case "controller":
			require("./create/controller")(args);
			break;
		case "a":
		case "action":
			require("./create/action")(args);
			break;
		case "m":
		case "model":
			require("./create/model")(args);
			break;
		case "s":
		case "service":
			require("./create/service")(args);
			break;
		case "f":
		case "facade":
			require("./create/facade")(args);
			break;

		default:
			console.log("\n", what, "is not supported\n");
			prompt();
			break;
	}
};