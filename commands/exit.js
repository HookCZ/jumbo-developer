"use strict";

const {prompt} =  require("../helpers");

module.exports = function cmdExit() {
	if (!jdev.CLI) return;

	jdev.CLI.question("Are you sure you want to exit developer? (y/n): ", function (confirm) {
		if (confirm === "y") {
			jdev.CLI.close();
			process.exit(0);
			return;
		}

		prompt();
	});
};