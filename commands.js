"use strict";

//region Imports

const {checkProjectExists, createDir, logColor, prompt} =  require("./helpers");
const $fs = require("fs");
const $path = require("path");

const config = require("./config");
const Colors = require("./colors");

//endregion

global.jdev = {
	CLI: null,
	PROJ_DIR: null,
	DEV_DIR: __dirname,
	CLASS_EXTENSION: config.classExtension,
	TEMPLATE_EXTENSION: config.templatExtension,
	CODE_TEMPLATE_EXTENSION: ".jstemplate",
	TEMPLATE_TEMPLATE_EXTENSION: ".tpltemplate",
};


module.exports = {
	// Array of available commands
	availableCommands: "version (v); help; create project (c p); create controller (c c); create action (c a); create facade (c f); create service (c s); create model (c m); create subapp (c sub); exit".split("; "),


	/**
	 * Initialization of jumbo-developer utility
	 * It's gonna ask for project directory; resolve entered $path and prepare some stuff.
	 * @param refCLI
	 */
	initDeveloper: function initDeveloper(refCLI) {
		let CLI = refCLI;
		jdev.CLI = CLI;
		let cwd = process.cwd();

		CLI.question(`Enter your project base directory (press ENTER for '${cwd}'): `, function (path) {
			if (path.trim().length === 0) {
				path = cwd;
			}

			jdev.PROJ_DIR = $path.resolve(cwd, path);

			// Check if directory exists
			try {
				$fs.accessSync(jdev.PROJ_DIR, $fs.constants.R_OK | $fs.constants.W_OK);
			} catch (e) {
				// Directory not exists, create it
				createDir(jdev.PROJ_DIR);
				console.log("\nDirectory didn't exists, was created for you.");
			}

			logColor(Colors.FgGreen, "\nYour project directory is ", jdev.PROJ_DIR, "\n");

			// New prompt with project base directory
			// CLI.setPrompt(Colors.FgBlue + Colors.Bright + "PROJECT " + PROJ_DIR + ">" + Colors.Reset + " ");

			checkProjectExists();
			prompt();
		});
	},

	//<editor-fold desc="Commands">

	/**
	 * Return version of framework and developer utility
	 */
	cmdVersion: require("./commands/version"),

	/**
	 * Show HELP
	 */
	cmdHelp: require("./commands/help"),

	/**
	 * Create controller, action, model, service, ...
	 * @param args
	 */
	cmdCreate: require("./commands/create"),

	/**
	 * Exit CLI
	 */
	cmdExit: require("./commands/exit"),

	// TODO: Implement
	// /**
	//  * Open role management in browser (GUI)
	//  */
	// cmdRoleManager: require("./commands/role-manager")

	//</editor-fold>
};