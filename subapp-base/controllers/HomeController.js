/**
 * Default controller
 * @class HomeController
 * @memberOf App.SubApps.__subapp__.Controllers
 */
class HomeController extends App.Controllers.BaseController {

	/**
	 * Default action returning index page
	 */
	async actionIndex() {

		// Call default view for this action (templates/Home/index.tpl) and give it data with hello
		this.view({ hello: "Hello World! "});
	}

}

module.exports = HomeController;