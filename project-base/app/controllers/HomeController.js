const BaseController = require("./BaseController");

/**
 * Default controller
 * @class HomeController
 */
class HomeController extends BaseController {

	//noinspection JSUnusedGlobalSymbols
	/**
	 * Homepage
	 */
	async actionIndex() {
		return this.view({});
	}
}

module.exports = HomeController;