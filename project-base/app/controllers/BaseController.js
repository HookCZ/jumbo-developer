const {Controller} = require("jumbo-core/base/Controller");

/**
 * This is your BaseController which you can edit
 * @memberOf App.Controllers
 */
class BaseController extends Controller {
	// /**
	//  * Called before each action
	//  */
	// async beforeActions() {
	// }

	// Add your own methods...
}

module.exports = BaseController;